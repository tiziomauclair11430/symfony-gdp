<?php

namespace Container9cmD37a;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder43784 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerb7d7b = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties003f5 = [
        
    ];

    public function getConnection()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getConnection', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getMetadataFactory', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getExpressionBuilder', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'beginTransaction', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getCache', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getCache();
    }

    public function transactional($func)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'transactional', array('func' => $func), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'wrapInTransaction', array('func' => $func), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'commit', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->commit();
    }

    public function rollback()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'rollback', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getClassMetadata', array('className' => $className), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'createQuery', array('dql' => $dql), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'createNamedQuery', array('name' => $name), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'createQueryBuilder', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'flush', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'clear', array('entityName' => $entityName), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->clear($entityName);
    }

    public function close()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'close', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->close();
    }

    public function persist($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'persist', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'remove', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'refresh', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'detach', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'merge', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getRepository', array('entityName' => $entityName), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'contains', array('entity' => $entity), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getEventManager', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getConfiguration', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'isOpen', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getUnitOfWork', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getProxyFactory', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'initializeObject', array('obj' => $obj), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'getFilters', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'isFiltersStateClean', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'hasFilters', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return $this->valueHolder43784->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerb7d7b = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder43784) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder43784 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder43784->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__get', ['name' => $name], $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        if (isset(self::$publicProperties003f5[$name])) {
            return $this->valueHolder43784->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder43784;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder43784;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder43784;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder43784;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__isset', array('name' => $name), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder43784;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder43784;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__unset', array('name' => $name), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder43784;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder43784;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__clone', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        $this->valueHolder43784 = clone $this->valueHolder43784;
    }

    public function __sleep()
    {
        $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, '__sleep', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;

        return array('valueHolder43784');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerb7d7b = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerb7d7b;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerb7d7b && ($this->initializerb7d7b->__invoke($valueHolder43784, $this, 'initializeProxy', array(), $this->initializerb7d7b) || 1) && $this->valueHolder43784 = $valueHolder43784;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder43784;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder43784;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
