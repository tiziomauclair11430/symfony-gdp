<?php

namespace ContainerGCASd0q;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_ArvAwdTService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.arvAwdT' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.arvAwdT'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'produit' => ['privates', '.errored..service_locator.arvAwdT.App\\Entity\\Produits', NULL, 'Cannot autowire service ".service_locator.arvAwdT": it references class "App\\Entity\\Produits" but no such service exists.'],
        ], [
            'produit' => 'App\\Entity\\Produits',
        ]);
    }
}
