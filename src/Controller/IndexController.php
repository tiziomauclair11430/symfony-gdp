<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProduitsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name: 'index_index',methods:['GET'])]
    public function index(ProduitsRepository $productRepository)
    {
        $contex= array('titre' => 'coucou' , 
        'products'=> $productRepository->findAll(), 
        'showEdit'=> false
    );

        return $this->render('base.html.twig',$contex);
    }
}








?>